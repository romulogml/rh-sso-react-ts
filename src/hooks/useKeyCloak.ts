import { useContext } from "react"
import { KeyCloakContext } from "../contexts/KeyCloakProvider"

export const useKeyCloak = () => {
    const context = useContext(KeyCloakContext);
    return context;
}