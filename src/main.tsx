import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import { ReactKeycloakProvider } from '@react-keycloak/web'
import keyCloakInstance from './keycloakConfig.ts'
import { KeyCloakProvider } from './contexts/KeyCloakProvider.tsx'

ReactDOM.createRoot(document.getElementById('root')!).render(
  // <ReactKeycloakProvider authClient={keyCloakInstance}>
    <KeyCloakProvider authClient={keyCloakInstance}>
      <App />
    </KeyCloakProvider>
  // </ReactKeycloakProvider>,
)
