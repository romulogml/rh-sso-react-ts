import './App.css'
// import { useKeycloak } from '@react-keycloak/web'
import { useKeyCloak } from './hooks/useKeyCloak';

function App() {
  // const { keycloak } = useKeycloak();
  const {authenticated, keyCloakInstance} = useKeyCloak();

  return (
    <>
        <div>
      {!authenticated && (
        <button onClick={() => keyCloakInstance?.login()}>Initialize Keycloak and Login</button>
      )}

      {authenticated && (
        <div>
          <p>Welcome, {keyCloakInstance?.tokenParsed?.preferred_username}!</p>
          <button onClick={() => keyCloakInstance?.logout()}>Logout</button>
        </div>
      )}
    </div>
    </>
  )
}

export default App
