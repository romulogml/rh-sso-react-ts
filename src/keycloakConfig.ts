import Keycloak from 'keycloak-js';

const keyCloakInstance = new Keycloak({
    url: 'http://localhost:8080/auth/',
    realm: 'demo',
    clientId: 'test',
});

keyCloakInstance.onAuthSuccess = () => {
    console.log("--- Authentication Successful!")
}

keyCloakInstance.onAuthLogout = () => {
    console.log("--- User logged out!");
};

keyCloakInstance.onTokenExpired = () => {
    console.log("--- Token Expired!");
}

keyCloakInstance.updateToken(10).then(refreshed => {
    if(refreshed){
        console.log('Token was successfully refreshed');
    } else {
        console.log('Token is still valid');
    }
}).catch(() => {
    console.log('Failed to refresh the token, or the session has expired');
})

export default keyCloakInstance;